package nl.piraya.sonar.rules;

import javax.annotation.Nonnull;
import org.sonar.plugins.java.api.CheckRegistrar;
import org.sonarsource.api.sonarlint.SonarLintSide;

/**
 * Provide rules for source code analysis.
 *
 * @author Onno Scheffers
 */
@SonarLintSide
public class PirayaFileCheckRegistrar implements CheckRegistrar {
   /**
    * Register the classes that will be used to instantiate checks during analysis.
    */
   @Override
   public void register(@Nonnull final RegistrarContext registrarContext) {
      registrarContext.registerClassesForRepository(PirayaRulesDefinition.REPOSITORY_KEY, RulesList.getJavaChecks(), RulesList.getJavaTestChecks());
   }
}
