package nl.piraya.sonar.rules;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Nonnull;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.SymbolMetadata;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.VariableTree;

/**
 * Checks that all parameters of methods and constructors have either the javax.annotation.Nullable or the javax.annotation.Nonnull annotation.
 *
 * @author Onno Scheffers
 */
@Rule(key = "NullOrNonnullParametersCheck")
public class NullOrNonnullParametersCheck extends IssuableSubscriptionVisitor {
   @Override
   @Nonnull
   public List<Tree.Kind> nodesToVisit() {
      return Arrays.asList(Tree.Kind.METHOD, Tree.Kind.CONSTRUCTOR);
   }

   @Override
   public void visitNode(@Nonnull final Tree tree) {
      if (hasSemantic()) {
         MethodTree method = (MethodTree) tree;
         List<VariableTree> params = method.parameters();
         for(VariableTree param : params) {
            if(!param.type().symbolType().isPrimitive()) {
               SymbolMetadata metadata = param.symbol().metadata();
               if (!metadata.isAnnotatedWith("javax.annotation.Nullable") && !metadata.isAnnotatedWith("javax.annotation.Nonnull")) {
                  reportIssue(method, "All parameters of methods and constructors should either be @Nullable or @Nonnull.");
                  break;
               }
            }
         }
      }
   }
}
