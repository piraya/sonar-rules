package nl.piraya.sonar.rules;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Nonnull;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.Modifier;
import org.sonar.plugins.java.api.tree.ModifierKeywordTree;
import org.sonar.plugins.java.api.tree.ModifiersTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.VariableTree;

/**
 * Checks that all parameters of methods and constructors are final.
 *
 * @author Onno Scheffers
 */
@Rule(key = "FinalParametersCheck")
public class FinalParametersCheck extends IssuableSubscriptionVisitor {
   @Override
   @Nonnull
   public List<Tree.Kind> nodesToVisit() {
      return Arrays.asList(Tree.Kind.METHOD, Tree.Kind.CONSTRUCTOR);
   }

   @Override
   public void visitNode(@Nonnull final Tree tree) {
      if (hasSemantic()) {
         MethodTree method = (MethodTree) tree;
         List<VariableTree> params = method.parameters();
         for(VariableTree param : params) {
            boolean paramIsFinal = false;
            ModifiersTree modifiers = param.modifiers();
            for(ModifierKeywordTree modifierKeywordTree : modifiers.modifiers()) {
               if(Modifier.FINAL.equals(modifierKeywordTree.modifier())) {
                  paramIsFinal = true;
               }
            }
            if(!paramIsFinal) {
               reportIssue(method, "All parameters of methods and constructors should be final.");
               break;
            }
         }
      }
   }
}
