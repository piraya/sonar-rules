package nl.piraya.sonar.rules;

import java.util.Collections;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.sonar.check.Rule;
import org.sonar.java.checks.serialization.SerializableContract;
import org.sonar.java.model.ModifiersUtils;
import org.sonar.java.resolve.JavaType;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.semantic.Type;
import org.sonar.plugins.java.api.tree.AssignmentExpressionTree;
import org.sonar.plugins.java.api.tree.ClassTree;
import org.sonar.plugins.java.api.tree.ExpressionTree;
import org.sonar.plugins.java.api.tree.IdentifierTree;
import org.sonar.plugins.java.api.tree.Modifier;
import org.sonar.plugins.java.api.tree.ParameterizedTypeTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.TypeTree;
import org.sonar.plugins.java.api.tree.VariableTree;
import org.sonar.plugins.java.api.tree.WildcardTree;

/**
 * This started out as a copy of the original {@link org.sonar.java.checks.serialization.SerializableFieldInSerializableClassCheck} and is meant to
 * replace it in projects that use the Wicket web framework with Spring integration.
 * This implementation excludes properties annotated with the @SpringBean annotation.
 *
 * NOTE: Ideally we wouldn't have needed to duplicate/replace the original check and would have preferred to simply
 *       filter out occurrences of the original check if the SpringBean annotation is encountered, but this is not
 *       yet supported by the Sonar API.
 *
 *       See https://stackoverflow.com/questions/38095337/sonar-ignore-lombok-code-via-custom-annotation
 *       and https://jira.sonarsource.com/browse/SONARJAVA-1761
 */
@Rule(key = "SerializableNonSpringBeanFieldInSerializableClassCheck")
public class SerializableNonSpringBeanFieldInSerializableClassCheck extends IssuableSubscriptionVisitor {
   @Nonnull
   public List<Tree.Kind> nodesToVisit() {
      return Collections.singletonList(Tree.Kind.CLASS);
   }

   public void visitNode(@Nonnull final Tree tree) {
      if (hasSemantic()) {
         ClassTree classTree = (ClassTree) tree;
         if (isSerializable(classTree) && !SerializableContract.hasSpecialHandlingSerializationMethods(classTree)) {
            for (Tree member : classTree.members()) {
               if (member.is(Tree.Kind.VARIABLE)) {
                  checkVariableMember((VariableTree) member);
               }
            }
         }
      }
   }

   private void checkVariableMember(@Nonnull final VariableTree variableTree) {
      if (!isExcluded(variableTree)) {
         IdentifierTree simpleName = variableTree.simpleName();
         if (isCollectionOfSerializable(variableTree.type())) {
            if (!ModifiersUtils.hasModifier(variableTree.modifiers(), Modifier.PRIVATE)) {
               reportIssue(simpleName, "Make \"" + simpleName.name() + "\" private or transient.");
            } else if (isUnserializableCollection(variableTree.type().symbolType()) || initializerIsUnserializableCollection(variableTree.initializer())) {
               reportIssue(simpleName);
            }

            checkCollectionAssignments(variableTree.symbol().usages());
         } else {
            reportIssue(simpleName);
         }
      }

   }

   private static boolean initializerIsUnserializableCollection(@Nullable final ExpressionTree initializer) {
      return initializer != null && isUnserializableCollection(initializer.symbolType());
   }

   private static boolean isUnserializableCollection(@Nonnull final Type type) {
      return !type.symbol().isInterface() && isSubtypeOfCollectionApi(type) && !implementsSerializable(type);
   }

   private void checkCollectionAssignments(@Nonnull final List<IdentifierTree> usages) {

      for (IdentifierTree usage : usages) {
         Tree parentTree = usage.parent();
         if (parentTree != null && parentTree.is(Tree.Kind.ASSIGNMENT)) {
            AssignmentExpressionTree assignment = (AssignmentExpressionTree) parentTree;
            ExpressionTree expression = assignment.expression();
            if (usage.equals(assignment.variable()) && !expression.is(Tree.Kind.NULL_LITERAL) && isUnserializableCollection(expression.symbolType())) {
               reportIssue(usage);
            }
         }
      }

   }

   private void reportIssue(@Nonnull final IdentifierTree tree) {
      reportIssue(tree, "Make \"" + tree.name() + "\" transient or serializable.");
   }

   private static boolean isExcluded(@Nonnull final VariableTree variableTree) {
      return isStatic(variableTree) || isTransientSerializableOrInjected(variableTree);
   }

   private static boolean isCollectionOfSerializable(@Nonnull final Tree tree) {
      if (tree.is(Tree.Kind.PARAMETERIZED_TYPE)) {
         ParameterizedTypeTree typeTree = (ParameterizedTypeTree)tree;
         if (isSubtypeOfCollectionApi(typeTree.symbolType())) {
            return typeTree.typeArguments().stream().allMatch(SerializableNonSpringBeanFieldInSerializableClassCheck::isCollectionOfSerializable);
         }
      }

      return isSerializable(tree);
   }

   private static boolean isSubtypeOfCollectionApi(@Nonnull final Type type) {
      return type.isSubtypeOf("java.util.Collection") || type.isSubtypeOf("java.util.Map");
   }

   private static boolean isStatic(@Nonnull final VariableTree member) {
      return ModifiersUtils.hasModifier(member.modifiers(), Modifier.STATIC);
   }

   private static boolean isTransientSerializableOrInjected(@Nonnull final VariableTree member) {
      return ModifiersUtils.hasModifier(member.modifiers(), Modifier.TRANSIENT)
            || isSerializable(member.type())
            || member.symbol().metadata().isAnnotatedWith("javax.inject.Inject")
            || member.symbol().metadata().isAnnotatedWith("org.apache.wicket.spring.injection.annot.SpringBean");
   }

   private static boolean isSerializable(@Nonnull final Tree tree) {
      if (tree.is(Tree.Kind.ENUM, Tree.Kind.PRIMITIVE_TYPE)) {
         return true;
      } else if (tree.is(Tree.Kind.CLASS)) {
         Symbol.TypeSymbol symbol = ((ClassTree)tree).symbol();
         return implementsSerializable(symbol.type());
      } else if (!tree.is(Tree.Kind.EXTENDS_WILDCARD, Tree.Kind.SUPER_WILDCARD, Tree.Kind.UNBOUNDED_WILDCARD)) {
         return implementsSerializable(((TypeTree)tree).symbolType());
      } else {
         TypeTree bound = ((WildcardTree)tree).bound();
         return bound != null && implementsSerializable(bound.symbolType());
      }
   }

   private static boolean implementsSerializable(@Nullable final Type type) {
      if (type != null && !type.isUnknown() && !type.isPrimitive()) {
         if (type.isArray()) {
            return implementsSerializable(((Type.ArrayType)type).elementType());
         } else {
            if(!type.isClass() && !((JavaType) type).isTagged(15)) {
               return false;
            }
            // Check if Serializable
            return type.erasure().isSubtypeOf("java.io.Serializable");
         }
      } else {
         return true;
      }
   }
}
