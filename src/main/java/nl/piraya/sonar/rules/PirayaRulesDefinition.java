package nl.piraya.sonar.rules;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import javax.annotation.Nonnull;
import org.sonar.api.rule.RuleStatus;
import org.sonar.api.rules.RuleType;
import org.sonar.api.server.debt.DebtRemediationFunction;
import org.sonar.api.server.rule.RulesDefinition;
import org.sonar.api.server.rule.RulesDefinitionAnnotationLoader;
import org.sonar.api.utils.AnnotationUtils;
import org.sonar.squidbridge.annotations.RuleTemplate;

/**
 * Declare rule metadata in server repository of rules so you can see them in the "Rules" section of the Sonar web application.
 */
public class PirayaRulesDefinition implements RulesDefinition {
   /**
    * The key that identifies our custom repository of Java rules.
    */
   static final String REPOSITORY_KEY = "piraya-java";

   // don't change this because the path is hard coded in CheckVerifier
   private static final String RESOURCE_BASE_PATH = "/org/sonar/l10n/java/rules/squid";

   private final Gson gson = new Gson();

   @Override
   public void define(@Nonnull final Context context) {
      NewRepository repository = context.createRepository(REPOSITORY_KEY, "java").setName("Piraya Custom Rules");

      List<Class> checks = RulesList.getChecks();
      new RulesDefinitionAnnotationLoader().load(repository, checks.toArray(new Class[checks.size()]));

      for (Class ruleClass : checks) {
         newRule(ruleClass, repository);
      }
      repository.done();
   }

   private void newRule(@Nonnull final Class<?> ruleClass, @Nonnull final NewRepository repository) {
      org.sonar.check.Rule ruleAnnotation = AnnotationUtils.getAnnotation(ruleClass, org.sonar.check.Rule.class);
      if (ruleAnnotation == null) {
         throw new IllegalArgumentException("No Rule annotation was found on " + ruleClass);
      }
      String ruleKey = ruleAnnotation.key();
      if (ruleKey == null || ruleKey.isEmpty()) {
         throw new IllegalArgumentException("No key is defined in Rule annotation of " + ruleClass);
      }
      NewRule rule = repository.rule(ruleKey);
      if (rule == null) {
         throw new IllegalStateException("No rule was created for " + ruleClass + " in " + repository.key());
      }
      ruleMetadata(ruleClass, rule);
      rule.setTemplate(AnnotationUtils.getAnnotation(ruleClass, RuleTemplate.class) != null);
   }

   private void ruleMetadata(@Nonnull final Class<?> ruleClass, @Nonnull final NewRule rule) {
      String metadataKey = rule.key();
      org.sonar.java.RspecKey rspecKeyAnnotation = AnnotationUtils.getAnnotation(ruleClass, org.sonar.java.RspecKey.class);
      if (rspecKeyAnnotation != null) {
         metadataKey = rspecKeyAnnotation.value();
         rule.setInternalKey(metadataKey);
      }
      addHtmlDescription(rule, metadataKey);
      addMetadata(rule, metadataKey);
   }

   private void addMetadata(@Nonnull final NewRule rule, @Nonnull final String metadataKey) {
      URL resource = PirayaRulesDefinition.class.getResource(RESOURCE_BASE_PATH + "/" + metadataKey + "_java.json");
      if (resource != null) {
         RuleMetatada metatada = gson.fromJson(readResource(resource), RuleMetatada.class);
         rule.setSeverity(metatada.defaultSeverity.toUpperCase(Locale.US));
         rule.setName(metatada.title);
         rule.addTags(metatada.tags);
         rule.setType(RuleType.valueOf(metatada.type));
         rule.setStatus(RuleStatus.valueOf(metatada.status.toUpperCase(Locale.US)));
         if (metatada.remediation != null) {
            rule.setDebtRemediationFunction(metatada.remediation.remediationFunction(rule.debtRemediationFunctions()));
            rule.setGapDescription(metatada.remediation.linearDesc);
         }
      }
   }

   private void addHtmlDescription(@Nonnull final NewRule rule, @Nonnull final String metadataKey) {
      URL resource = PirayaRulesDefinition.class.getResource(RESOURCE_BASE_PATH + "/" + metadataKey + "_java.html");
      if (resource != null) {
         rule.setHtmlDescription(readResource(resource));
      }
   }

   @Nonnull
   private String readResource(@Nonnull final URL resource) {
      try (BufferedReader in = new BufferedReader(new InputStreamReader(resource.openStream()))) {
         StringBuilder result = new StringBuilder();
         String line;
         while ((line = in.readLine()) != null) {
            result.append(line).append("\n");
         }
         return result.toString();
      } catch (IOException e) {
         throw new IllegalStateException("An exception occurred while reading resource: " + resource, e);
      }
   }

   private static class RuleMetatada {
      String title;
      String status;
      Remediation remediation;

      String type;
      String[] tags;
      String defaultSeverity;
   }

   private static class Remediation {
      String func;
      String constantCost;
      String linearDesc;
      String linearOffset;
      String linearFactor;

      @Nonnull
      private DebtRemediationFunction remediationFunction(@Nonnull final DebtRemediationFunctions drf) {
         if (func.startsWith("Constant")) {
            return drf.constantPerIssue(constantCost.replace("mn", "min"));
         }
         if ("Linear".equals(func)) {
            return drf.linear(linearFactor.replace("mn", "min"));
         }
         return drf.linearWithOffset(linearFactor.replace("mn", "min"), linearOffset.replace("mn", "min"));
      }
   }
}
