package nl.piraya.sonar.rules;

import javax.annotation.Nonnull;
import org.sonar.api.Plugin;

/**
 * Entry-point of the plugin. This class adds the extensions into Sonar.
 *
 * @author Onno Scheffers
 */
public class PirayaRulesPlugin implements Plugin {
   @Override
   public void define(@Nonnull final Context context) {
      // server extensions, instantiated during server startup
      context.addExtension(PirayaRulesDefinition.class);

      // batch extensions, instantiated during code analysis
      context.addExtension(PirayaFileCheckRegistrar.class);
   }
}
