package nl.piraya.sonar.rules;

import java.util.Collections;
import java.util.List;
import javax.annotation.Nonnull;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.SymbolMetadata;
import org.sonar.plugins.java.api.semantic.Type;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.TypeTree;

/**
 * Checks that all methods that return an object have either the javax.annotation.Nullable or the javax.annotation.Nonnull annotation.
 *
 * @author Onno Scheffers
 */
@Rule(key = "NullOrNonnullMethodCheck")
public class NullOrNonnullMethodCheck extends IssuableSubscriptionVisitor {
   @Override
   @Nonnull
   public List<Tree.Kind> nodesToVisit() {
      return Collections.singletonList(Tree.Kind.METHOD);
   }

   @Override
   public void visitNode(@Nonnull final Tree tree) {
      if (hasSemantic()) {
         MethodTree method = (MethodTree) tree;
         TypeTree typeTree = method.returnType();
         Type returnType = typeTree == null ? null : typeTree.symbolType();
         if(returnType != null && !returnType.isVoid() && !returnType.isPrimitive()) {
            SymbolMetadata metadata = method.symbol().metadata();
            if (!metadata.isAnnotatedWith("javax.annotation.Nullable") && !metadata.isAnnotatedWith("javax.annotation.Nonnull")) {
               reportIssue(method, "All methods that return an object should either be @Nullable or @Nonnull.");
            }
         }
      }
   }
}
