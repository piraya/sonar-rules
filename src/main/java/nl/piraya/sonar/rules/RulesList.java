package nl.piraya.sonar.rules;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;
import org.sonar.plugins.java.api.JavaCheck;

/**
 * Convenience util-class that lists all Java checks and all test checks.
 *
 * @author Onno Scheffers
 */
final class RulesList {
   /**
    * Private constructor since this is a util-class with only static methods.
    */
   private RulesList() {
   }

   /**
    * Returns a list of all Java checks and Test checks combined.
    *
    * @return A list of all Java checks and all Test checks combined.
    */
   @Nonnull
   static List<Class> getChecks() {
      List<Class> list = new ArrayList<>(1);
      list.addAll(getJavaChecks());
      list.addAll(getJavaTestChecks());
      return list;
   }

   /**
    * Returns a list of all Java checks.
    *
    * @return A list of all Java checks.
    */
   @Nonnull
   static List<Class<? extends JavaCheck>> getJavaChecks() {
      List<Class<? extends JavaCheck>> list = new ArrayList<>(1);
      list.add(FinalParametersCheck.class);
      list.add(NullOrNonnullMethodCheck.class);
      list.add(NullOrNonnullParametersCheck.class);
      list.add(SerializableNonSpringBeanFieldInSerializableClassCheck.class);
      return list;
   }

   /**
    * Returns a list of all Test checks.
    *
    * @return A list of all Test checks.
    */
   @Nonnull
   static List<Class<? extends JavaCheck>> getJavaTestChecks() {
      return new ArrayList<>(0);
   }
}
