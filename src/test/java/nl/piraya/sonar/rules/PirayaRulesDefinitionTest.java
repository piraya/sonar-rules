package nl.piraya.sonar.rules;

import org.junit.Test;
import org.sonar.api.server.rule.RulesDefinition;
import org.sonar.api.server.rule.RulesDefinition.Param;
import org.sonar.api.server.rule.RulesDefinition.Rule;

import static org.junit.Assert.*;

/**
 * Tests the {@link PirayaRulesDefinition} class.
 *
 * @author Onno Scheffers
 */
public class PirayaRulesDefinitionTest {

   @Test
   public void test() {
      // Lookup our repository
      PirayaRulesDefinition rulesDefinition = new PirayaRulesDefinition();
      RulesDefinition.Context context = new RulesDefinition.Context();
      rulesDefinition.define(context);
      RulesDefinition.Repository repository = context.repository(PirayaRulesDefinition.REPOSITORY_KEY);
      assertNotNull(repository);

      // Verify the repository
      assertEquals("Piraya Custom Rules", repository.name());
      assertEquals("java", repository.language());
      assertEquals(RulesList.getChecks().size(), repository.rules().size());

      // Make sure all parameters of all rules have a description
      for (Rule rule : repository.rules()) {
         for (Param param : rule.params()) {
            assertNotNull(param.description());
            assertFalse(param.description().isEmpty());
         }
      }
   }
}
