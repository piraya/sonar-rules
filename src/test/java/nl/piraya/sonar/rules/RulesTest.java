package nl.piraya.sonar.rules;

import java.io.File;
import java.util.Collections;
import javax.annotation.Nonnull;
import org.junit.Test;
import org.sonar.java.checks.verifier.JavaCheckVerifier;
import org.sonar.plugins.java.api.JavaFileScanner;

/**
 * Tests all the custom rules in this plugin.
 *
 * @author Onno Scheffers
 */
public class RulesTest {
   /**
    * Tests the FinalParametersCheck rule on the file 'FinalParameterTestFile.java'.
    */
   @Test
   public void testFinalParameterCheck() {
      verify("FinalParameterTestFile.java", new FinalParametersCheck());
   }

   /**
    * Tests the NullOrNonnullParametersCheck rule on the file 'NullOrNonnullParameterTestFile.java'.
    */
   @Test
   public void testNullOrNonnullParameterCheck() {
      verify("NullOrNonnullParameterTestFile.java", new NullOrNonnullParametersCheck());
   }

   /**
    * Tests the NullOrNonnullMethodCheck rule on the file 'NullOrNonnullMethodTestFile.java'.
    */
   @Test
   public void testNullOrNonnullMethodCheck() {
      verify("NullOrNonnullMethodTestFile.java", new NullOrNonnullMethodCheck());
   }

   /**
    * Tests the SerializableNonSpringBeanFieldInSerializableClassCheck rule on the file 'SpringBeanTestFile.java'.
    */
   @Test
   public void testSerializableNonSpringBeanFieldInSerializableClassCheck() {
      verify("SpringBeanTestFile.java", new SerializableNonSpringBeanFieldInSerializableClassCheck());
   }

   /**
    * Convenience method that calls the Sonar JavaCheckVerifier to validate the custom rule against the given file.
    *
    * @param file The name of the file in the src/test/files folder.
    * @param rule The rule instance to use for testing.
    */
   private void verify(@Nonnull final String file, @Nonnull final JavaFileScanner rule) {
      JavaCheckVerifier.verify("src/test/files/" + file, rule, Collections.singletonList(new File("target/classes")));
   }
}
