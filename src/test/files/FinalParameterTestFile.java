/**
 * This file is used from the {@link RulesTest} unit-test to validate the {@link FinalParameterCheck} rule. This file is a valid Java
 * file that breaks the Sonar rule in some places. The exected non-compliant lines have an inline comment that will be picked up by the Sonar
 * unit-test-framework while verifying.
 *
 * @author Onno Scheffers
 */
public class FinalParameterTestFile {
   public FinalParameterTestFile(final float param1, final String param2) {}

   public FinalParameterTestFile(int param1, Object param2) {} // Noncompliant {{All parameters of methods and constructors should be final.}}

   public void doStuffWith(final byte param1, final byte[] param2, final String param3) {}

   public void doStuffWith(long param1, String[] param2, Object param3) {} // Noncompliant {{All parameters of methods and constructors should be final.}}

   public void doStuffWith(final Object param1, String param3) {} // Noncompliant {{All parameters of methods and constructors should be final.}}
}
