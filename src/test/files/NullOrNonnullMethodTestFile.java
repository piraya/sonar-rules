import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * This file is used from the {@link RulesTest} unit-test to validate the {@link NullOrNonnullMethodCheck} rule. This file is a valid Java file that breaks
 * the Sonar rule in some places. The exected non-compliant lines have an inline comment that will be picked up by the Sonar unit-test-framework
 * while verifying.
 *
 * @author Onno Scheffers
 */
public class NullOrNonnullMethodTestFile {
   public void voidIsAlwaysValid() {}

   public int primitiveIsAlwaysValid() {
      return 1;
   }

   public Integer wrapperRequiresAnnotation() { // Noncompliant {{All methods that return an object should either be @Nullable or @Nonnull.}}
      return 1;
   }

   public String stringRequiresAnnotation() { // Noncompliant {{All methods that return an object should either be @Nullable or @Nonnull.}}
      return "HelloWorld";
   }

   public byte[] arrayRequiresAnnotation() { // Noncompliant {{All methods that return an object should either be @Nullable or @Nonnull.}}
      return new byte[0];
   }

   @Nullable
   public Integer wrapperWithAnnotationIsValid() {
      return 1;
   }

   @Nonnull
   public String stringWithAnnotationIsValid() {
      return "HelloWorld";
   }

   @Nonnull
   public byte[] arrayWithAnnotationIsValid() {
      return new byte[0];
   }
}
