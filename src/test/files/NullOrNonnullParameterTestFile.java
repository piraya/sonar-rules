import javax.annotation.Nullable;
import javax.annotation.Nonnull;

/**
 * This file is used from the {@link RulesTest} unit-test to validate the {@link NullOrNonNullParameterCheck} rule. This file is a
 * valid Java file that breaks the Sonar rule in some places. The exected non-compliant lines have an inline comment that will be picked up by the Sonar
 * unit-test-framework while verifying.
 *
 * @author Onno Scheffers
 */
public class NullOrNonnullParameterTestFile {
   public NullOrNonnullParameterTestFile(float param1, @Nullable String param2) {}

   public NullOrNonnullParameterTestFile(int param1, Object param2) {} // Noncompliant {{All parameters of methods and constructors should either be @Nullable or @Nonnull.}}

   public void doStuffWith(byte param1, @Nonnull byte[] param2, @Nullable String param3) {}

   public void doStuffWith(long param1, String[] param2, Object param3) {} // Noncompliant {{All parameters of methods and constructors should either be @Nullable or @Nonnull.}}

   public void doStuffWith(@Nonnull Object param1, String param3) {} // Noncompliant {{All parameters of methods and constructors should either be @Nullable or @Nonnull.}}
}
