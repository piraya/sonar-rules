import java.io.Serializable;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 * This file is used from the {@link RulesTest} unit-test to validate the {@link SerializableNonSpringBeanFieldInSerializableClassCheck} rule. This file
 * is a valid Java file that breaks the Sonar rule in some places. The exected non-compliant lines have an inline comment that will be picked up by the
 * Sonar unit-test-framework while verifying.
 *
 * @author Onno Scheffers
 */
public class SpringBeanTestFile implements Serializable {
   private Object o1; // Noncompliant {{Make "o1" transient or serializable.}}

   @SpringBean
   private Object o2;
}
