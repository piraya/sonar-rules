### Piraya Custom Rules for Sonar

This project contains custom Sonar rules we use internally at [Piraya B.V.](https://www.piraya.nl) to
help improve code quality.
 
This project was originally based on the
[Sonar custom rules example project](https://github.com/SonarSource/sonar-custom-rules-examples/tree/master/java-custom-rules), so
it uses the same [LGPL license](./LICENSE).

This project contains the following rules:

* FinalParameterCheck
  This rule checks if all parameters of all methods and constructors are `final`.

* NullOrNonnullParametersCheck
  This rule checks if all non-primitive parameters of all methods and constructors have either the `@Nullable` annotation
  or the `@Nonnull` annotation.

* NullOrNonnullMethodCheck
  This rule checks if all methods that return an object have either the `@Nullable` annotation
  or the `@Nonnull` annotation.

* SerializableNonSpringBeanFieldInSerializableClassCheck
  This rule replaces the default [SerializableFieldInSerializableClassCheck](https://github.com/SonarSource/sonar-java/blob/master/java-checks/src/main/java/org/sonar/java/checks/serialization/SerializableFieldInSerializableClassCheck.java)
  rule (squid:S1948) with a rule that excludes properties annotated with the
  `@SpringBean` annotation that comes with the [Wicket-Spring integration](https://cwiki.apache.org/confluence/display/WICKET/Spring).
